#ifndef AI_H
#define AI_H

#include "Board.h"

enum MODE {DUMB,MEDIUM,SMART, CHEATER};

class AI {
private:
    MODE mode;
    // This value stores last position that was a hit
    std::pair<int,int> hit_center = {-1,-1};
    std::string direction = "none";
    // This one keeps the last position shot at
    std::pair<int,int> last_pos = {-1,-1};
    // This one keeps the next position to shoot at
    std::pair<int,int> next_pos = {-1,-1};
public:
    AI(){};
    AI(MODE mode){
        this -> mode = mode;
    }
    std::pair<int,int> nextMove(Board& board);
    void dumbStrategy(Board& board);
    void mediumStrategy(Board& board);
    void smartStrategy(Board& board);
    void cheaterStrategy(Board& board);
    void goUp(Board& board);
    void goRight(Board& board);
    void goDown(Board& board);
    void goLeft(Board& board);
    std::pair<int,int> getRandomPos(Board& board);
    std::string lastPos();
};



#endif
