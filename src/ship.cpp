#include "ship.h"

Ship::Ship(int size, std::pair<int,int> pos0, int dir) {
    // Upon initialization, the Ship object should check that
    // it's starting position and direction is possible given
    // the current state of the board

    // Get starting position of ship

    // Get direction of ship from valid directions
    // Here we need a function that checks if the ship can be placed up, down, left or right from starting position

    // Add ship coordinates
    switch (dir)
    {
    case 0:
        // up
        for (int i = 0; i < size; ++i) {
            coords.push_back({pos0.first - i, pos0.second});
        }
        break;
    case 1:
        // right
        for (int i = 0; i < size; ++i) {
            coords.push_back({pos0.first, pos0.second + i});
        }
        break;
    case 2:
        // down
        for (int i = 0; i < size; ++i) {
            coords.push_back({pos0.first + i, pos0.second});
        }
        break;
    case 3:
        // left
        for (int i = 0; i < size; ++i) {
            coords.push_back({pos0.first, pos0.second - i});
        }
        break;
    default:
        break;
    }
}
