#ifndef SHIP_H
#define SHIP_H

#include <iostream>
#include <vector>

class Ship {
    public:
        Ship(){};
        Ship(int size, std::pair<int,int> pos0, int dir);
        std::vector<std::pair<int,int>> coords;
};

#endif