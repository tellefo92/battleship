#ifndef BOARD_H
#define BOARD_H

#include "ship.h"
#include <vector>
#include <iostream>

const int COLS = 10;
const int ROWS = 10;

class Board {
private:
    std::vector<std::vector<int>> board;
    int hit_squares = 0;
    int filled_squares = 0;

public:
    Board(){initBoard();}
    void initBoard();
    void displayBoard(bool show);
    void hit(std::pair<int,int> pos);
    bool hittable(std::pair<int,int> pos);
    bool hasLost();
    void remaining();
    bool placeShip(Ship &ship);
    bool isValidPos(std::pair<int,int> pos);
    bool succesfulHit(std::pair<int,int> pos);
    std::vector<std::pair<int,int>> availPos();
    bool beenShotAt(std::pair<int,int> pos);
    bool isShip(std::pair<int,int> pos);
    void placeRandom(std::vector<std::pair<int,int>>& available, int size);
    
};


#endif 
