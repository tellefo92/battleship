#ifndef GAME_H
#define GAME_H

#include "Board.h"
#include "AI.h"

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
 
class Game {
    private:
        std::string _gameid;
        int _mode;
        int turn_no = 0;
        bool random_board;
        int ai_difficulty;
        std::string winner;
    // Tellef
    public:
        Game();
        void setGameid();
        //void setMode(int mode);
        void start();
        void getPos(std::string pos, std::pair<int,int>& pos0);
        void getDir(int& dir);
        void recordStats();
        void displayStats();
        std::string makeString(std::string desc, std::string value);

};

#endif