#include "game.h"

Game::Game() {
    // Create a "pseudo" random game id
    setGameid();
    std::cout << "Welcome to battleship.." << std::endl;
    while (true) {
        std::cout << "Please select mode:" << std::endl;
        std::cout << "(1) Milton Bradley \n(2) Hasbro" << std::endl;
        std::cout << "Answer: ";
        std::cin >> _mode;
        if (_mode == 1 || _mode == 2) {
            break;
        } else {
            std::cout << "Invalid mode! \n \n";
        }
    }
    system("clear");
    while (true) {
        std::cout << "Would you like to \n(1) Place boats yourself \n(2) Get a random board" << std::endl;
        std::cout << "Answer: ";
        int ans;
        std::cin >> ans;
        if (ans == 1) {
            random_board = false;
            break;
        } else if (ans == 2) {
            random_board = true;
            break;
        } else {
            std::cout << "Not a valid response! \n \n";
        }
    }
    system("clear");
    while(true) {
        std::cout << "What AI difficulty would you like to be matched up against?\n(1) Easy\n(2) Medium" << std::endl;
        std::cout << "Answer: ";
        std::cin >> ai_difficulty;
        if (ai_difficulty == 1 || ai_difficulty == 2) {
            break;
        } else {
            std::cout << "Not a valid response! \n \n";
        }
    }
}

void Game::setGameid() {
    // Function to create a pseudo unique string and assign it to the _gameid
    std::string id;
    for (size_t i = 0; i < 10; ++i) {
        if (i == 5) {
            id += '-';
        }
        char c;
        int r1 = std::rand() % 10 + 48;
        int r2 = std::rand() % 25 + 97;
        if (std::rand() % 2) {
            c = r1;
        } else {
            c = r2;
        }
        id += c;
    }
    _gameid = id;
}

void Game::start() {
    // Creating boards
    Board player_board = Board();
    Board computer_board = Board();    
    // Create AI instance
    AI ai;
    if (ai_difficulty == 1) {
        ai = AI(DUMB);
    } else {
        ai = AI(MEDIUM);
    }
    // Variables to store ship starting position, size and direction
    std::pair<int,int> pos0;
    std::string pos;
    int dir;
    std::vector<int> ship_sizes = {5, 4, 3, 3, 2};
    std::vector<std::string> ship_names;
    if (_mode == 1) {
        ship_names = {"Carrier", "Battleship", "Cruiser", "Submarine", "Destroyer"};
    } else {
        ship_names = {"Carrier", "Battleship", "Destroyer", "Submarine", "Patrol Boat"};
    }
    int remaining_ships = ship_sizes.size();
    // Create ships
    for (auto size: ship_sizes) {
        // If player wants to select positions themselves
        if (!random_board) {
            system("clear");
            std::cout << "-------- Your ships ----------\n\n";
            player_board.displayBoard(true);
            std::cout << "Remaining ships to be placed: " << remaining_ships << std::endl;
            std::cout << "Input position for" << ship_names[0] << " ship of size " << size << ": ";
            ship_names.erase(ship_names.begin());
            std::cin >> pos;
            getPos(pos, pos0);
            getDir(dir);
            Ship ship = Ship(size, pos0, dir);
            while(!player_board.placeShip(ship)) {
                std::cout << "Can't put a ship there!" << std::endl;
                getPos(pos, pos0);
                getDir(dir);
                ship = Ship(size, pos0, dir);
            }
            remaining_ships -= 1;
        } else {
            // If player rather get a random board
            std::vector<std::pair<int,int>> available = player_board.availPos();
            player_board.placeRandom(available, size);
        }
        // generate computer board
        std::vector<std::pair<int,int>> available = computer_board.availPos();
        computer_board.placeRandom(available, size);
    }
    bool running = true;
    bool player_turn = true;
    bool successful_hit;
    bool cheat = false;
    // Run the game
    while(running) {
        if (player_turn) {
            //PLAYER TURN
            // Update board so game state is presented for the player
            system("clear");
            std::cout << "--- Computer board ----" << std::endl;
            computer_board.displayBoard(cheat);
            std::cout << "\n----- Your board ------" << std::endl;
            player_board.displayBoard(true);
            turn_no += 1;
            if (turn_no > 1) {
                std::cout << "Computer fired at " << ai.lastPos() << std::endl;
            }
            std::cout << "Where would you like to shoot?" << std::endl;
            std::cin >> pos;
            if (pos == "cheat") {
                cheat = true;
                system("clear");
                std::cout << "--- Computer board ----" << std::endl;
                computer_board.displayBoard(cheat);
                std::cout << "\n----- Your board ------" << std::endl;
                player_board.displayBoard(true);
                std::cout << "Where would you like to shoot?" << std::endl;
                std::cin >> pos;
            } else if (pos == "end") {
                winner = "ended";
                running = false;
            }
            getPos(pos, pos0);
            while(!computer_board.hittable(pos0)) {
                std::cout << "That square has already been hit!" << std::endl;
                std::cout << "Select another one: " << std::endl;
                std::cin >> pos;
                getPos(pos, pos0);
            }
            computer_board.hit(pos0);
            if (computer_board.hasLost()) {
                std::cout << "You won!" << std::endl;
                winner = "Player";
                running = false;
            }
            player_turn = false;
        } else {
            // COMPUTER TURN
            pos0 = ai.nextMove(player_board);
            std::cout << pos0.first << pos0.second << std::endl;
            player_board.hit(pos0);
            if (player_board.hasLost()) {
                winner = "Computer";
                running = false;
            }
           player_turn = true;
        }
    }
    recordStats();
}

void Game::getPos(std::string pos, std::pair<int,int>& pos0) {
    // Converting string position to pair
    int x = pos[0] - 97;
    int y = -1;
    if (pos.length() == 3) {
        y = 9;
    } else {
        y = pos[1] - '1';
    }
    pos0 = {y,x};
}

void Game::getDir(int& dir) {
    // Function to get direction of ship from user
    std::cout << "Input ship direction (up=0, right=1, down=2, left=3): ";
    std::cin >> dir;
    if (dir < 0 || dir > 3) {
        std::cout << "Not a valid direction!" << std::endl;
        getDir(dir);
    }
}

void Game::recordStats() {
    // Function to record game stats to ../data/player_stats.txt
    std::ofstream outfile;
    outfile.open("../data/player_stats.txt", std::ios_base::app);
    std::string mode;
    if (_mode == 1) {
        mode = "Milton-Bradley";
    } else {
        mode = "Hasbro";
    }
    outfile << "-\nGameid: " << _gameid << "\nMode: " << mode << "\nTurns: " << turn_no << "\nWinner: " << winner << "\n";
    outfile.close();
}

void Game::displayStats() {
    // Function to read stats from ../data/player_stats.txt and display them for the user
    system("clear");
    std::ifstream infile("../data/player_stats.txt");
    std::string placeholder, gameid, mode, winner;
    int turns;
    int tot_turns = 0;
    int wins = 0;
    int losses = 0;
    int games = 0;
    int avg_turns_wins = 0;
    double win_percent;
    while(infile >> placeholder >> gameid) {
        games += 1;
        infile >> placeholder;
        infile >> placeholder >> mode;
        infile >> placeholder >> turns;
        infile >> placeholder >> winner;
        tot_turns += turns;
        if (winner == "Player") {
            wins += 1;
            avg_turns_wins += turns;
        } else {
            losses += 1;
        }
    }
    win_percent = wins / (double) games * 100;
    if (wins > 0) {
        avg_turns_wins = avg_turns_wins / wins + 1;
    }
    std::cout << "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n";
    std::cout << "┃                    Game statistics                    ┃\n";
    std::cout << "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫\n";
    std::cout << "┃                    Previous game                      ┃\n";
    std::cout << "┠───────────────────────────────────────────────────────┨\n";
    std::cout << makeString("Game ID:", _gameid);
    std::cout << makeString("Turns:", std::to_string(turns));
    std::cout << makeString("Winner:", winner);
    std::cout << "┠───────────────────────────────────────────────────────┨\n";
    std::cout << "┃                   Lifetime stats                      ┃\n";
    std::cout << "┠───────────────────────────────────────────────────────┨\n";
    std::cout << makeString("Total games:", std::to_string(games));
    std::cout << makeString("Wins:", std::to_string(wins));
    std::cout << makeString("Losses:", std::to_string(losses));
    std::cout << makeString("Win %:", std::to_string(win_percent));
    std::cout << makeString("Average turns per win:", std::to_string(avg_turns_wins));
    std::cout << "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n";
    std::cout << std::flush;
}

std::string Game::makeString(std::string desc, std::string value) {
    std::string s = "┃ " + desc;
    for (size_t i = s.length(); i < 57 - value.length(); ++i) {
        s += " ";
    }
    s += value;
    s += " ┃\n";
    return s;
}
