# PROJECT DESCRIPTION

## Requirements
Requires `cmake` and `cpp` <br />

## Usage
Clone the repository using
```sh
$ git clone https://gitlab.com/tellefo92/<REPO NAME>.git
```
then navigate into the folder using
```sh
$ cd <FOLDER NAME>
```
Uses Cmake to build:
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../
# Generate makefile for debug build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug      
# Compile the project
$ make
#Run the compiled binary
$ ./main
```
To use **CHEAT** mode, simply type the word `cheat` when the game asks you where you would like to hit. <br />
To end the game, type the word `end` when the game asks you where you would like to hit. <br />
When the game has ended, you will be asked if you want to see statistics of the game, and if you would like to play another round.

## Notes on 'opponent'
- The *easy* opponent difficulty randomly shoots at a position that hasn't been shot at before.
- The *medium* opponent shoots randomly at positions until it hits a position occupied by a ship. Then it performs a depth-first search from that position until it either hits a position not occupied by a ship or a wall, in all 4 directions from the initial hit position. When it has checked all directions, it randomly selects another position again.

## Contributions
The logic behind the game was discussed, and even though we started on separate tasks we quickly realized we had to be much more in sync and working on each others task. The classes were created as below, but most of the decisions were discussed together.

Tellef: <br /> Most of Game class <br /> Ship class <br /><br />
Miriam: <br /> Most of Board class <br /> AI class 
<br /><br />


## Maintainers
Tellef Østereng | [@tellefo92](https://gitlab.com/tellefo92)
Miriam Lagergren| [@MiriamL](https://gitlab.com/MiriamL)
