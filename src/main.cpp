#include "game.h"
#include "time.h"

int main() {
    // Game loop
    while(true) {
        system("clear");
        std::srand(time(NULL));
        Game game = Game();
        game.start();
        std::cout << "Thank you for playing!" << std::endl;
        std::cout << "Game stats recorded." << std::endl;
        std::cout << "Would you like to see game statistics (yes/no)?" << std::endl;
        std::string ans;
        std::cin >> ans;
        if (ans == "yes" || ans == "y") {
            game.displayStats();
        }
        std::cout << "Would you like to play again (yes/no)?" << std::endl;
        std::cin >> ans;
        if (ans == "yes") {
            continue;
        } else {
            break;
        }
   }
   return 0;
}