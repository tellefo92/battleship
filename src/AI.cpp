#include "AI.h"

std::pair<int,int> AI::nextMove(Board& board) {
    last_pos = next_pos;
    if (mode == DUMB) {
        dumbStrategy(board);
    } else if (mode == MEDIUM) {
        mediumStrategy(board);
    } else if (mode == SMART) {
        smartStrategy(board);
    } else if (mode == CHEATER) {
        cheaterStrategy(board);
    }
    return next_pos;
}

void AI::dumbStrategy(Board& board) {
    next_pos = getRandomPos(board);
}

void AI::mediumStrategy(Board& board) {
    // This strategy uses DFS to search whenever it has hit a square, 
    // all the way until it either misses or reaches a boarder, then changes direction clockwise.
    if (last_pos.first != -1 && hit_center.first == -1) {
        if (board.succesfulHit(last_pos)) {
            hit_center = last_pos;
            direction = "up";
        }
    }
    if (hit_center.first == -1) {
        next_pos = getRandomPos(board);
        return;
    }
    if (direction == "up") {
        goUp(board);
    } else if (direction == "right") {
        goRight(board);
    } else if (direction == "down") {
        goDown(board);
    } else if (direction == "left") {
        goLeft(board);
    }
    return;
}

void AI::smartStrategy(Board& board) {
    std::cout << "I am very smart" << std::endl;
}

void AI::cheaterStrategy(Board& board) {
    std::cout << "I'm a cheat" << std::endl;
    /*
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (board[i][j] == 1) {
                next_pos = board[i][j];
                return;
            }
        }
    }
    */
}

void AI::goUp(Board& board) {
    if (last_pos.first == 0 || !board.succesfulHit(last_pos)) {
        last_pos = hit_center;
        direction = "right";
        goRight(board);
        return;
    }
    next_pos = {last_pos.first - 1, last_pos.second};
    if (!board.hittable(next_pos)) {
        goRight(board);
    }
}

void AI::goRight(Board& board) {
    if (last_pos.second == 9 || !board.succesfulHit(last_pos)) {
        last_pos = hit_center;
        direction = "down";
        goDown(board);
        return;
    }
    next_pos = {last_pos.first, last_pos.second + 1};
    if (!board.hittable(next_pos)) {
        goDown(board);
    }
}

void AI::goDown(Board& board) {
    if (last_pos.first ==  9 || !board.succesfulHit(last_pos)) {
        last_pos = hit_center;
        direction = "left";
        goLeft(board);
        return;
    }
    next_pos = {last_pos.first + 1, last_pos.second};
    if (!board.hittable(next_pos)) {
        goLeft(board);
    }
}

// Called if direction of search is left
void AI::goLeft(Board& board) {
    if (last_pos.second == 0 || !board.succesfulHit(last_pos)) {
        direction = "none";
        hit_center = {-1,-1};
        next_pos = getRandomPos(board);
        return;
    }
    next_pos = {last_pos.first, last_pos.second - 1};
    if (!board.hittable(next_pos)) {
        next_pos = getRandomPos(board);
    }
}

// Returns a random position
std::pair<int,int> AI::getRandomPos(Board& board){
    int randomx;
    int randomy;
    randomx = rand() % 10;
    randomy = rand() % 10;
    if (board.hittable({randomx, randomy})) {
        return {randomx, randomy};
    } else {
        return getRandomPos(board);
    }
    
}

// Returns a string with the last position
std::string AI::lastPos() {
    std::string lastpos;
    lastpos += next_pos.second + 'a';
    if (next_pos.first + '1' == ':') {
        lastpos += "10";
    } else {
        lastpos += next_pos.first + '1';
    }
    return lastpos;
}