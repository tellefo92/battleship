#include "Board.h"

// Initializing the board
void Board::initBoard() {
    for (int i = 0; i < COLS; i++) {
        std::vector<int> b;
        for (int j = 0; j < ROWS; j++) {
            b.push_back(0);
        }
        board.push_back(b);
    }
}

// Prints board to the console
void Board::displayBoard(bool show) {
    std::cout << "___A_B_C_D_E_F_G_H_I_J_" << std::endl;
    for (int i = 0; i < COLS; i++) {
        if (i < 9) {
            std::cout << " " << i + 1 << "|";
        } else {
            std::cout << i + 1 << "|";
        }
        for (int j = 0; j < ROWS; j++) {
            switch (board[i][j])
            {
                case 0 : std::cout << "_|";
                    break;
                case 1 : 
                    if (show) {
                        std::cout << "S|";
                    } else {
                        std::cout << "_|";
                    }
                    break;
                case 2 : std::cout << "X|";
                    break;
                case 3 : std::cout << "O|";
                    break;
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// Will take a postion and do a hit on theboard
void Board::hit(std::pair<int,int> pos){
    if (board[pos.first][pos.second] == 1) {
        hit_squares += 1;
    }
    board[pos.first][pos.second] += 2;
}

// Checks if the position is hittable
bool Board::hittable(std::pair<int,int> pos) {
    if (board[pos.first][pos.second] < 2) {
        return true;
    }
    return false;
}

// bool showing if user lost
bool Board::hasLost() {
    if (hit_squares == filled_squares) {
        return true;
    }
    return false;
}

// Shows remaining squares to hit
void Board::remaining() {
    std::cout << hit_squares << " hit, " << filled_squares - hit_squares << " remaining." << std::endl;
}

// Places ship and returns bool if valid
bool Board::placeShip(Ship &ship) {
    for (size_t i = 0; i < ship.coords.size(); i++) {
        if (!isValidPos(ship.coords[i])) {
            return false;
        }
    }
    for (size_t i = 0; i < ship.coords.size(); i++) {
        board[ship.coords[i].first][ship.coords[i].second] = 1;
        filled_squares += 1;
        }
    return true;
}

// Checks if a position is valid
bool Board::isValidPos(std::pair<int,int> pos){
    if (pos.first > 0 || pos.second > 0 || pos.first < 9 || pos.second < 9) {
        if (board[pos.first][pos.second] == 0) {
            return true;
        }
    }
    return false;
}

bool Board::succesfulHit(std::pair<int,int> pos) {
    if (board[pos.first][pos.second] == 3) {
        return true;
    }
    return false;
}

// Returns a vector of all the available positions (empty positions)
std::vector<std::pair<int,int>> Board::availPos() {
    std::vector<std::pair<int,int>> available;
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (board[i][j] == 0) {
                available.push_back({i,j});
            }
        }
    }
    return available;
}

// Checks if the postition has already been shot at
bool Board::beenShotAt(std::pair<int,int> pos) {
    if (board[pos.first][pos.second] > 2){
        return true;
    } else return false;
}

// Checks if there is a ship on a position
bool Board::isShip(std::pair<int,int> pos) {
    if (board[pos.first][pos.second] == 1){
        return true;
    } else return false;
}

// Places a random ship by using available positions
void Board::placeRandom(std::vector<std::pair<int,int>>& available, int size) {
    int rindex = std::rand() % available.size();
    std::vector<int> directions;
    std::pair<int,int> pos0 = available[rindex];
    available.erase(available.begin() + rindex);
    // check if boat can be placed up
    if (pos0.first - size >= 0) {
        directions.push_back(0);
    }
    // check if boat can be placed down
    if (pos0.first + size <= 9) {
        directions.push_back(2);
    }
    // check if boat can be placed left
    if (pos0.second - size >= 0) {
        directions.push_back(3);
    }
    // check if boat can be placed right
    if (pos0.first + size >= 9) {
        directions.push_back(1);
    }
    if (directions.size() == 0) {
        placeRandom(available, size);
    }
    int rdir = directions[std::rand() % directions.size()];
    Ship ship = Ship(size, pos0, rdir);
    if (!placeShip(ship)) {
        placeRandom(available, size);
    }
    return;
}

